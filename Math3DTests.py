from Math3D import *

def SumDiffTest():
  
  a = Vector3D(0, 0, 0)
  b = Vector3D(0, 0, 5)

  sum = Vector3D.add(a, b)
  sum2 = Vector3D.add(b, a)
  diff = Vector3D.sub(a, b)
  diff2 = Vector3D.sub(b, a)

  assert sum.x == 0
  assert sum.y == 0
  assert sum.z == 5

  assert sum2.x == 0
  assert sum2.y == 0
  assert sum2.z == 5

  assert diff.x == 0
  assert diff.y == 0
  assert diff.z == -5

  assert diff2.x == 0
  assert diff2.y == 0
  assert diff2.z == 5

  a = Vector3D(50, 50, 50)
  b = Vector3D(100, 75, 25)

  sum = Vector3D.add(a, b)
  sum2 = Vector3D.add(b, a)
  diff = Vector3D.sub(a, b)
  diff2 = Vector3D.sub(b, a)

  assert sum.x == 150
  assert sum.y == 125
  assert sum.z == 75

  assert sum2.x == 150
  assert sum2.y == 125
  assert sum2.z == 75

  assert diff.x == -50
  assert diff.y == -25
  assert diff.z == 25

  assert diff2.x == 50
  assert diff2.y == 25
  assert diff2.z == -25

  print("Passed SumDiffTest.")

def ScaleTest():
  c = Vector3D(1, 1, 1)
  c = Vector3D.scale(c, 5)

  assert c.x == 5
  assert c.y == 5
  assert c.z == 5
  
  c = Vector3D(3, 4, 5)
  c = Vector3D.scale(c, 2)

  assert c.x == 6
  assert c.y == 8
  assert c.z == 10

  c = Vector3D(3, 4, 5)
  c = Vector3D.scale(c, 1)
  
  assert c.x == 3
  assert c.y == 4
  assert c.z == 5

  print("Passed ScaleTest")

def DotTest():

  a = Vector3D(1, 0, 0)
  b = Vector3D(0, 1, 0)
  c = Vector3D(0, 0, 1)


  dot = Vector3D.dot(a, a)
  assert dot == 1

  dot = Vector3D.dot(b, b)
  assert dot == 1

  dot = Vector3D.dot(c, c)
  assert dot == 1

  dot = Vector3D.dot(a, b)
  assert dot == 0

  dot = Vector3D.dot(b, c)
  assert dot == 0

  dot = Vector3D.dot(a, c)
  assert dot == 0

  a = Vector3D(1, 1, 1)
  b = Vector3D(2, 2, 2)
  c = Vector3D(1, 2, 3)

  dot = Vector3D.dot(a, a)
  assert dot == 3
  
  dot = Vector3D.dot(b, b)
  assert dot == 12
  
  dot = Vector3D.dot(c, c)
  assert dot == 14
  
  dot = Vector3D.dot(a, b)
  assert dot == 6
  
  dot = Vector3D.dot(b, c)
  assert dot == 12
  
  dot = Vector3D.dot(a, c)
  assert dot == 6

  print("Passed DotTest")

def MagTest():

  a = Vector3D(3, 4, 0)
  b = Vector3D(0, 5, 12)
  c = Vector3D(7, 0, 24)

  mag = a.magnitude()
  assert mag == 5

  mag = b.magnitude()
  assert mag == 13

  mag = c.magnitude()
  assert mag == 25

  print("Passed MagTest")

def NormTest():

  a = Vector3D(3, 4, 0)
  a = Vector3D.normalize(a)

  assert "{0:.2f}".format(a.x) == "0.60"
  assert "{0:.2f}".format(a.y) == "0.80"

  print("Passed NormTest")

def CmpTest():

  a = Vector3D(0, 0, 0)
  assert Vector3D.cmp(a, Vector3D(0,0,0))
  assert not Vector3D.cmp(a, Vector3D(1,0,0))
  assert not Vector3D.cmp(a, Vector3D(0,1,0))
  assert not Vector3D.cmp(a, Vector3D(0,0,1))
  
  b = Vector3D(5, 5, 5)
  assert Vector3D.cmp(b, Vector3D(5,5,5))
  assert not Vector3D.cmp(b, Vector3D(4,5,5))
  assert not Vector3D.cmp(b, Vector3D(5,5,4))
  assert not Vector3D.cmp(b, Vector3D(5,4,5))

  print("Passed CmpTest")

def MaxTest():

  a = Vector3D(-10, 100, 0)
  b = Vector3D(-9, 50, -1)
  c = Vector3D(-15, 3000, 2)

  x = Vector3D.max(a,b)
  y = Vector3D.max(a,c)
  z = Vector3D.max(b,c)

  assert Vector3D.cmp(x, Vector3D(-9, 100, 0))
  assert Vector3D.cmp(y, Vector3D(-10, 3000, 2))
  assert Vector3D.cmp(z, Vector3D(-9, 3000, 2))

  print("Passed MaxTest")


