import math

class Vector3D():

  def __init__(self, x, y, z):
    self.x = x
    self.y = y
    self.z = z


  def add(vec1, vec2):
    
    result = Vector3D(0,0,0)

    result.x = vec1.x + vec2.x
    result.y = vec1.y + vec2.y
    result.z = vec1.z + vec2.z

    return result

  def sub(vec1, vec2):
    
    result = Vector3D(0,0,0)

    result.x = vec1.x - vec2.x
    result.y = vec1.y - vec2.y
    result.z = vec1.z - vec2.z

    return result

  def scale(vec, scalar):
    
    result = Vector3D(0,0,0)

    result.x = vec.x * scalar
    result.y = vec.y * scalar
    result.z = vec.z * scalar

    return result

  def dot(vec1, vec2):
    
    dot = 0

    dot += vec1.x * vec2.x
    dot += vec1.y * vec2.y
    dot += vec1.z * vec2.z

    return dot

  def magnitude(self):
    return math.sqrt(Vector3D.dot(self, self))

  def normalize(vec):
    
    normal = Vector3D(vec.x, vec.y, vec.z)
    normal = Vector3D.scale(normal, 1.0 / normal.magnitude())

    return normal

  # Builds a new vector from the max of each component of vec1 and vec2
  def max(vec1, vec2):
  
      rslt = Vector3D(0,0,0)
      
      rslt.x = max(vec1.x, vec2.x)
      rslt.y = max(vec1.y, vec2.y)
      rslt.z = max(vec1.z, vec2.z)

      return rslt


  #Tests whether the two vectors are equivalent, for testing purposes
  #Does NOT round or otherwise manipulate floating points
  def cmp(vec1, vec2):

    val = True
    
    val = val and vec1.x == vec2.x
    val = val and vec1.y == vec2.y
    val = val and vec1.z == vec2.z

    return val



