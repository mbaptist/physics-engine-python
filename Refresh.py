
#printing
print("Hello World.")

#Variables
myVar = 2.0
print(myVar)

#If Statement
if myVar == 2.0:
  print("Statement true!")

#While loop
while True:
  print("This is an infinite loop.")

#Lists
myList = []
myList.append(myVar)
print(len(myList))
myList.remove(myVar)

#Classes
class MyClass():
  def __init__(self, name):
    self.myName = name

  def getName(self):
    return self.myName

newInstance = MyClass("Matt")
print(newInstance.getName)
