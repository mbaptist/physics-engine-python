from Math3D import *
from PhysicsEngine import *

eng = PhysicsEngine()

particle = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
eng.add(particle)

f = Force(Vector3D(1,0,0))
f.add(particle)
eng.addForce(f)


for i in range(180):
  eng.integrate(1/60.)

eng.printObjs()