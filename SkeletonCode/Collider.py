import Math

class IntersectData:

  def __init__(self, doesIntersect, dist):
    
    """   Your code goes here!   """
    pass

class BoundingGeometry:

  def intersect(self, other):
    
    """   Your code goes here!   """
    pass

  def intersectSphere():
    
    """   Your code goes here!   """
    pass

  def intersectAABB():
    
    """   Your code goes here!   """
    pass

class BoundingSphere(BoundingGeometry):

  def __init__(self, pos, rad):
    
    """   Your code goes here!   """
    pass

  def update(self, deltaP):
    
    """   Your code goes here!   """
    pass

  def intersectSphere(self, sphere):

    """   Your code goes here!   """
    pass

class AABB(BoundingGeometry):

  def __init__(self, minExt, maxExt):
    
    """   Your code goes here!   """
    pass
  
  def update(deltaP):
    
    """   Your code goes here!   """
    pass
  
  def intersectAABB(self, other):

    """   Your code goes here!   """
    pass
