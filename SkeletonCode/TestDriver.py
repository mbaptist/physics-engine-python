from Math3DTests import *
from EngineTests import *

def Math3DTests():
  print("------Math3DTests------")
  try:
    SumDiffTest()
  except:
    print("Failed SumDiffTest")

  try:
    ScaleTest()
  except:
    print("Failed ScaleTest")

  try:
    DotTest()
  except:
    print("Failed DotTest")

  try:
    MagTest()
  except:
    print("Failed MagTest")

  try:
    NormTest()
  except:
    print("Failed NormTest")

  try:
    CmpTest()
  except:
    print("Failed CmpTest")

  try:
    MaxTest()
  except:
    print("Failed MaxTest")

def EngineTests():
  print("------EngineTests------")
  try:
    intTest()
  except:
    print("Failed IntTest")

  try:
    PhysicsEngineTest()
  except:
    print("Failed PhysicsEngineTest")

  try:
    ForceTest()
  except:
    print("Failed ForceTest")

  try:
    TotalTest()
  except:
    print("Failed TotalTest")

  try:
    aabbColliderTest()
  except:
    print("Failed aabbColliderTest")

Math3DTests()
EngineTests()
