from Math3D import Vector as Vector3D

class PhysicsEngine():
  
  def __init__(self):
    
    """   Your code goes here!   """
    pass

  def computeForces(self, delta):
    
    """   Your code goes here!   """
    pass
  
  def integrate(self, delta):
    
    """   Your code goes here!   """
    pass

  def add(self, obj):
    
    """   Your code goes here!   """
    pass

  def addForce(self, force):
    
    """   Your code goes here!   """
    pass

  def size(self):
    
    """   Your code goes here!   """
    pass

  def printObjs(self):
    
    """   Your code goes here!   """
    pass

class PhysicsObject():
  
  def __init__(self, pos, vel, mass):
    
    """   Your code goes here!   """
    pass
  
  def setCollider(self, collider):
    
    """   Your code goes here!   """
    pass

  def integrate(self, delta):
    
    """   Your code goes here!   """
    pass



class Force():
  
  def __init__(self, force):
    
    """   Your code goes here!   """
    pass
  
  def add(self, obj):
    
    """   Your code goes here!   """
    pass

  def applyForce(self):
    
    """   Your code goes here!   """
    pass

  def integrate(self, delta):
    
    """   Your code goes here!   """
    pass
