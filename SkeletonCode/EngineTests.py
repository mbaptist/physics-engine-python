from PhysicsEngine import *
from Math import *
from Collider import *

from Math3D import Vector as Vector3D

def intTest():

  #Constant Velocity in 1D, Timestep: 1
  #a,b,c all begin at origin (0,0,0)
  #a,b,c have a constant velocity of 1 in R

  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,1), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,1,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(1,0,0), 1)

  a.integrate(1)
  b.integrate(1)
  c.integrate(1)

  assert Vector3D.cmp(a.pos, Vector3D(0,0,1))
  assert Vector3D.cmp(b.pos, Vector3D(0,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(1,0,0))

  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))

  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,2))
  assert Vector3D.cmp(b.pos, Vector3D(0,2,0))
  assert Vector3D.cmp(c.pos, Vector3D(2,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))

  a.integrate(1)
  b.integrate(1)
  c.integrate(1)

  assert Vector3D.cmp(a.pos, Vector3D(0,0,3))
  assert Vector3D.cmp(b.pos, Vector3D(0,3,0))
  assert Vector3D.cmp(c.pos, Vector3D(3,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))
  
  #Constant Velocity in 3D, TimeStep: 1
  #a,b,c all begin at origin (0,0,0)
  #a,b,c have a constant velocity in R3

  a = PhysicsObject(Vector3D(0,0,0), Vector3D(1,1,1), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(3,1,3), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(1,3,1), 1)
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(1,1,1))
  assert Vector3D.cmp(b.pos, Vector3D(3,1,3))
  assert Vector3D.cmp(c.pos, Vector3D(1,3,1))
  
  assert Vector3D.cmp(a.vel, Vector3D(1,1,1))
  assert Vector3D.cmp(b.vel, Vector3D(3,1,3))
  assert Vector3D.cmp(c.vel, Vector3D(1,3,1))
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(2,2,2))
  assert Vector3D.cmp(b.pos, Vector3D(6,2,6))
  assert Vector3D.cmp(c.pos, Vector3D(2,6,2))
  
  assert Vector3D.cmp(a.vel, Vector3D(1,1,1))
  assert Vector3D.cmp(b.vel, Vector3D(3,1,3))
  assert Vector3D.cmp(c.vel, Vector3D(1,3,1))
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(3,3,3))
  assert Vector3D.cmp(b.pos, Vector3D(9,3,9))
  assert Vector3D.cmp(c.pos, Vector3D(3,9,3))
  
  assert Vector3D.cmp(a.vel, Vector3D(1,1,1))
  assert Vector3D.cmp(b.vel, Vector3D(3,1,3))
  assert Vector3D.cmp(c.vel, Vector3D(1,3,1))
  
  #Constant Velocity in 1D, Timestep: .5
  #a,b,c all begin at origin (0,0,0)
  #a,b,c have a constant velocity of 1 in R
  
  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,1), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,1,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(1,0,0), 1)
  
  a.integrate(.5)
  b.integrate(.5)
  c.integrate(.5)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,.5))
  assert Vector3D.cmp(b.pos, Vector3D(0,.5,0))
  assert Vector3D.cmp(c.pos, Vector3D(.5,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))
  
  a.integrate(.5)
  b.integrate(.5)
  c.integrate(.5)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,1))
  assert Vector3D.cmp(b.pos, Vector3D(0,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(1,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))
  
  a.integrate(.5)
  b.integrate(.5)
  c.integrate(.5)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,1.5))
  assert Vector3D.cmp(b.pos, Vector3D(0,1.5,0))
  assert Vector3D.cmp(c.pos, Vector3D(1.5,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))
  
  #Constant Force in 1D, Timestep: 1, Mass: 1
  #a,b,c all begin at origin (0,0,0)
  #a,b,c have a constant force of 1 in R

  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
  
  a.force = Vector3D(0,0,1)
  b.force = Vector3D(0,1,0)
  c.force = Vector3D(1,0,0)
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,1))
  assert Vector3D.cmp(b.pos, Vector3D(0,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(1,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,3))
  assert Vector3D.cmp(b.pos, Vector3D(0,3,0))
  assert Vector3D.cmp(c.pos, Vector3D(3,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,2))
  assert Vector3D.cmp(b.vel, Vector3D(0,2,0))
  assert Vector3D.cmp(c.vel, Vector3D(2,0,0))
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(0,0,6))
  assert Vector3D.cmp(b.pos, Vector3D(0,6,0))
  assert Vector3D.cmp(c.pos, Vector3D(6,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,3))
  assert Vector3D.cmp(b.vel, Vector3D(0,3,0))
  assert Vector3D.cmp(c.vel, Vector3D(3,0,0))

  print("Passed IntTest")

def PhysicsEngineTest():
  
  eng = PhysicsEngine()
  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,1), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,1,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(1,0,0), 1)

  eng.add(a)
  eng.add(b)
  eng.add(c)

  assert eng.size() == 3

  eng.integrate(1)

  assert Vector3D.cmp(a.pos, Vector3D(0,0,1))
  assert Vector3D.cmp(b.pos, Vector3D(0,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(1,0,0))
  
  assert Vector3D.cmp(a.vel, Vector3D(0,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(1,0,0))

  print("Passed PhysicsEngineTest")

def ForceTest():

  f = Force(Vector3D(1,0,0))

  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,1), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,1,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(1,0,0), 1)

  f.add(a)
  f.add(b)
  f.add(c)

  f.applyForce()
  
  a.integrate(1)
  b.integrate(1)
  c.integrate(1)

  assert Vector3D.cmp(a.pos, Vector3D(1,0,1))
  assert Vector3D.cmp(b.pos, Vector3D(1,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(2,0,0))

  assert Vector3D.cmp(a.vel, Vector3D(1,0,1))
  assert Vector3D.cmp(b.vel, Vector3D(1,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(2,0,0))
  
  assert Vector3D.cmp(a.force, Vector3D(1,0,0))
  assert Vector3D.cmp(b.force, Vector3D(1,0,0))
  assert Vector3D.cmp(c.force, Vector3D(1,0,0))

  print("Passed ForceTest")

def TotalTest():

  f = Force(Vector3D(1,0,0))
  g = Force(Vector3D(0,1,0))
  h = Force(Vector3D(0,0,1))

  a = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
  b = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)
  c = PhysicsObject(Vector3D(0,0,0), Vector3D(0,0,0), 1)

  f.add(a)
  g.add(b)
  h.add(c)

  eng = PhysicsEngine()
  
  eng.add(a)
  eng.add(b)
  eng.add(c)

  eng.addForce(f)
  eng.addForce(g)
  eng.addForce(h)

  eng.integrate(1)

  assert Vector3D.cmp(a.pos, Vector3D(1,0,0))
  assert Vector3D.cmp(b.pos, Vector3D(0,1,0))
  assert Vector3D.cmp(c.pos, Vector3D(0,0,1))
  
  assert Vector3D.cmp(a.vel, Vector3D(1,0,0))
  assert Vector3D.cmp(b.vel, Vector3D(0,1,0))
  assert Vector3D.cmp(c.vel, Vector3D(0,0,1))
  
  assert Vector3D.cmp(a.force, Vector3D(1,0,0))
  assert Vector3D.cmp(b.force, Vector3D(0,1,0))
  assert Vector3D.cmp(c.force, Vector3D(0,0,1))

  eng.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(3,0,0))
  assert Vector3D.cmp(b.pos, Vector3D(0,3,0))
  assert Vector3D.cmp(c.pos, Vector3D(0,0,3))
  
  assert Vector3D.cmp(a.vel, Vector3D(2,0,0))
  assert Vector3D.cmp(b.vel, Vector3D(0,2,0))
  assert Vector3D.cmp(c.vel, Vector3D(0,0,2))
  
  assert Vector3D.cmp(a.force, Vector3D(1,0,0))
  assert Vector3D.cmp(b.force, Vector3D(0,1,0))
  assert Vector3D.cmp(c.force, Vector3D(0,0,1))

  eng.integrate(1)
  
  assert Vector3D.cmp(a.pos, Vector3D(6,0,0))
  assert Vector3D.cmp(b.pos, Vector3D(0,6,0))
  assert Vector3D.cmp(c.pos, Vector3D(0,0,6))
  
  assert Vector3D.cmp(a.vel, Vector3D(3,0,0))
  assert Vector3D.cmp(b.vel, Vector3D(0,3,0))
  assert Vector3D.cmp(c.vel, Vector3D(0,0,3))
  
  assert Vector3D.cmp(a.force, Vector3D(1,0,0))
  assert Vector3D.cmp(b.force, Vector3D(0,1,0))
  assert Vector3D.cmp(c.force, Vector3D(0,0,1))

  print("Passed TotalTest")


def aabbColliderTest():

  engine = PhysicsEngine()

  a = PhysicsObject(Vector3D(0,1,0), Vector3D(0,0,0), 1)
  b = PhysicsObject(Vector3D(1,0,0), Vector3D(0,0,0), 1)
  c = PhysicsObject(Vector3D(0,0,1), Vector3D(0,0,0), 1)

  a.setCollider(AABB(Vector3D(-.5, -.5, -.5), Vector3D(.5, .5, .5)))
  b.setCollider(AABB(Vector3D(-.5, -.5, -.5), Vector3D(.5, .5, .5)))
  c.setCollider(AABB(Vector3D(-.5, -.5, -.5), Vector3D(.5, .5, .5)))

  engine.add(a)
  engine.add(b)
  engine.add(c)

  engine.integrate(1)

  collisions = engine.collisions
  
  assert len(collisions) == 0

  print("Passed aabbColliderTest")
