import math
from Math import Vector

class Vector():

  def __init__(self, x, y, z):
    
    this.x = x
    this.y = y
    this.z = z
  
  def add(vec1, vec2):
    
    """   Your code goes here!   """
    pass
  
  def sub(vec1, vec2):
    
    """   Your code goes here!   """
    pass
  
  def scale(vec, scalar):
    
    """   Your code goes here!   """
    pass
  
  def dot(vec1, vec2):
    
    
    """   Your code goes here!   """
    pass
  
  def magnitude(self):
    
    """   Your code goes here!   """
    pass
  
  def normalize(vec):
    
    """   Your code goes here!   """
    pass
  
  # Builds a new vector from the max of each component of vec1 and vec2
  def max(vec1, vec2):
  
    rslt = Vector(0,0,0)
      
    rslt.x = max(vec1.x, vec2.x)
    rslt.y = max(vec1.y, vec2.y)
    rslt.z = max(vec1.z, vec2.z)

    return rslt

  #Tests whether the two vectors are equivalent, for testing purposes
  #Does NOT round or otherwise manipulate floating points
  def cmp(vec1, vec2):

    val = True
    
    val = val and vec1.x == vec2.x
    val = val and vec1.y == vec2.y
    val = val and vec1.z == vec2.z

    return val

