import Math3D

class IntersectData:

  def __init__(self, doesIntersect, dist):
    self.doesIntersect = doesIntersect
    self.pdepth = dist

class BoundingGeometry:

  def intersect(self, other):
    if type(other) == BoundingSphere:
      self.intersectSphere(other)
    elif type(other) == AABB:
      self.intersectAABB(other)

  def intersectSphere():
    return None

  def intersectAABB():
    return None

class BoundingSphere(BoundingGeometry):

  def __init__(self, pos, rad):
    self.pos = pos #Vector3d
    self.rad = rad #Vector3d
  
  def update(self, deltaP):
    Vector3D.add(self.pos, deltaP)

  def intersectSphere(self, sphere):

    dist = Vector3D.sub(sphere.pos, self.pos)
    dist = Vector3D.magnitude(dist)

    radiusDist = sphere.radius + self.radius
    
    return IntersectData(dist < radiusDist, dist - radiusDist)

class AABB(BoundingGeometry):

  def __init__(self, minExt, maxExt):
    self.minExt = minExt #Vector3D
    self.maxExt = maxExt #Vector3D
  
  def update(deltaP):
    Vector3D.add(self.minExt, deltaP)
    Vector3D.add(self.maxExt, deltaP)

  def intersectAABB(self, other):

    dist1 = Vector3D.sub(other.minExt - self.maxExt)
    dist2 = Vector3D.sub(self.minExt - other.maxExt)

    greatestDist = Vector3D.max(dist1, dist2)

    sep = max(greatestDist.x, greatestDist.y, greatestDist.z)

    return IntersectData(sep < 0, greatestDist)

