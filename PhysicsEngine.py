from Math3D import *

class PhysicsEngine():
  
  def __init__(self):
    self.objs = []
    self.forces = []

  def computeForces(self, delta):
    for x in self.objs:
      x.force = Vector3D(0,0,0)
    
    for x in self.forces:
      x.integrate(delta)
      x.applyForce()

  def integrate(self, delta):
    #1) Compute Forces
    self.computeForces(delta)
    
    #2) Calculate Motion
    for x in self.objs:
      x.integrate(delta)

    #3) Detect Collisions
    collisions = []
    
    for i in range(len(self.objs)):
      for j in range(i, len(self.objs)):
        rigidBody1 = self.objs[i]
        rigidBody2 = self.objs[j]
        
        if rigidBody1.collider and rigidBody2.collider:
          intersectData = rigidBody1.collider.intersect(rigidBody2.collider)
        
          #If there is a collision
          if intersectData.doesIntersect == True:
            collisions.append(intersectData)

    #4) Apply Collision Resolution
    # NOT IMPLEMENTED

  def add(self, obj):
      self.objs.append(obj)

  def addForce(self, force):
      self.forces.append(force)

  def size(self):
    return len(self.objs)

  def printObjs(self):
    for x in range(len(self.objs)):
      obj = self.objs[x]
      print("Object:", x, "has:")
      print("Position: <" + str(obj.pos.x) + ",", str(obj.pos.y) + ",", str(obj.pos.z) + ">")
      print("Velocity: <" + str(obj.vel.x) + ",", str(obj.vel.y) + ",", str(obj.vel.z) + ">")

class PhysicsObject():
  def __init__(self, pos, vel, mass):
    self.pos = pos #Vector3D
    self.vel = vel #Vector3D
    self.force = Vector3D(0,0,0)
    self.mass = mass
  
    self.collider = None
  
  def setCollider(self, collider):
    self.collider = collider

  def integrate(self, delta):
    self.vel = Vector3D.add(self.vel, Vector3D.scale(self.force, delta / self.mass))
    self.pos = Vector3D.add(self.pos, Vector3D.scale(self.vel, delta))

    if self.collider:
      self.collider.update(Vector3D.scale(self.vel, delta))



class Force():
  def __init__(self, force):
    self.force = force
    self.objs = []

  def add(self, obj):
    self.objs.append(obj)

  def applyForce(self):
    for x in self.objs:
      x.force = Vector3D.add(x.force, self.force)

  def integrate(self, delta):
    #Optional, leave blank for now
    pass
