from PhysicsEngine import Force

class Gravity(Force):
  def applyForce(self, delta):
    for x in self.objs:
      x.force += 9.8 * x.mass
